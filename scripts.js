const books = [
  { author: "Люсі Фолі", name: "Список запрошених", price: 70 },
  { author: "Сюзанна Кларк", name: "Джонатан Стрейндж і м-р Норрелл" },
  { name: "Дизайн. Книга для недизайнерів.", price: 70 },
  { author: "Алан Мур", name: "Неономікон", price: 70 },
  { author: "Террі Пратчетт", name: "Рухомі картинки", price: 40 },
  { author: "Анґус Гайленд", name: "Коти в мистецтві" },
];
let requiredFields = ["author", "name", "price"];
let root = document.querySelector(".root");
let ul = document.createElement("ul");
validation(books, requiredFields);
function validation(books, requiredFields) {
  books.forEach((book) => {
    try {
      if (requiredFields.every((f) => Object.keys(book).includes(f))) {
        creation(book);
      } else {
        throw new Error(
          `Книга не має всіх обов'язкових полів: ${requiredFields.filter(
            (f) => !Object.keys(book).includes(f)
          )}.`
        );
      }
    } catch (e) {
      console.log(e.message);
    }
  });
}
function creation(book) {
  let bookInfo = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}`;
  let li = document.createElement("li");
  li.innerText = bookInfo;
  ul.appendChild(li);
}
root.append(ul);
